import { Component, OnInit,OnDestroy } from '@angular/core';
import { AppService } from '../service/App.service';
import { Subscription} from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit,OnDestroy {
  takesub:Subscription;
  usesub:Subscription;

  TakeTool:any[]=[];
  UseTool:any[]=[];
  type = 'PieChart';
 /* data = [
     ['Firefox', 45.0],
     ['IE', 26.8],
     ['Chrome', 12.8],
     ['Safari', 8.5],
     ['Opera', 6.2],
     ['Others', 0.7] 
  ];*/
  columnNames = ['Module', 'Percentage'];
  options = {   
    is3D:true 
  };
  width = 550;
  height = 400;
  constructor(private apps:AppService) { }

  ngOnInit() {
    this.getDateFromService();
  }
ngOnDestroy()
{
  this.takesub.unsubscribe();
  this.usesub.unsubscribe();
}

  getDateFromService(): void {
  this.takesub=  this.apps.getThevalues("TakeTools")
        .subscribe(Tools => this.TakeTool = Tools);
 this.usesub=   this.apps.getThevalues("useTools")
        .subscribe(Tools => this.UseTool = Tools);    

    
  }
}
