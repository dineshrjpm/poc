import { Injectable } from '@angular/core';
import { MasterElement } from '../Model/Model';
import { Observable, of ,Subject} from 'rxjs';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

/*const TakeTool_DATA: MasterElement[] = [
    { ModuleName: 'Visable?', Nooftools: 5},
    { ModuleName: 'Accessible', Nooftools: 7},
    { ModuleName: 'Earily Passable', Nooftools: 3}
    
  ];

  const UseTool_DATA: MasterElement[] = [
    { ModuleName: 'Shelf Height', Nooftools: 12,},
    { ModuleName: 'Table height', Nooftools: 10},
    { ModuleName: 'Tool availablity', Nooftools: 13}
    
  ];
*/
@Injectable({
  providedIn: 'root',
})
export class AppService {
  
    dataSourceTT:MasterElement[]=[];
    dataSourcUT:MasterElement[] = [];
   // private listSubject = new Subject<MasterElement[]>();
   // private listObservable = this.listSubject.asObservable();

  constructor(private httpService: HttpClient) {

    this.httpService.get('./assets/Taketools.json').subscribe(
      data => {
        this.dataSourceTT = data as MasterElement [];	 
         // console.log(this.dataSourceTT[1]);
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );

    this.httpService.get('./assets/UseTools.json').subscribe(
      data => {
        this.dataSourcUT = data as MasterElement [];	 
         // console.log(this.dataSourceTT[1]);
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );



  }


  getTheTools(ToolType:string): Observable<MasterElement[]> {
        if(ToolType=="TakeTools")
        {
      
          return of(this.dataSourceTT);
        }
        else
        return of(this.dataSourcUT);
   
  }
  updateModule(ToolType:string,index:number,ModuleName:string,Nooftools:number)
  {
    if(ToolType=="TakeTools")
    {
       this.dataSourceTT[index].ModuleName=ModuleName;
       this.dataSourceTT[index].Nooftools=Nooftools
    }
    else{
      this.dataSourcUT[index].ModuleName=ModuleName;
      this.dataSourcUT[index].ModuleName=ModuleName;
    }
  }
  DeleteModule(ToolType:string,temp:MasterElement): Observable<MasterElement[]>
  {
    if(ToolType=="TakeTools")
    {
      const index: number = this.dataSourceTT.indexOf(temp);
      if (index !== -1) {
          this.dataSourceTT.splice(index, 1);
      }   
      return of(this.dataSourceTT);     
    }
    else{
      const index: number = this.dataSourcUT.indexOf(temp);
      if (index !== -1) {
          this.dataSourcUT.splice(index, 1);
      }  
      return of(this.dataSourcUT);      
      
    }
  }
  



  UpdateToolSum(ToolType:string,index:number,Toolsum:number,ElementSummary:string)
  {
    if(ToolType=="TakeTools")
    {
       this.dataSourceTT[index].sum=Toolsum;
       this.dataSourceTT[index].ToolSummary=ElementSummary
    }
    else{
      this.dataSourcUT[index].sum=Toolsum;
      this.dataSourcUT[index].ToolSummary=ElementSummary
    }

  }

  getThevalues(ToolType:string): Observable<any[]> {
    let temp:any[]=[];
    if(ToolType=="TakeTools")
    {

      for(let i=0;i<this.dataSourceTT.length;i++)
        {
        temp.push([this.dataSourceTT[i].ModuleName,this.dataSourceTT[i].sum]);
        }
    return of(temp);
    }
    else{
    for(let i=0;i<this.dataSourcUT.length;i++)
        {
        temp.push([this.dataSourcUT[i].ModuleName,this.dataSourcUT[i].sum]);

        }
    return of(temp);
    }

}

 


}