import { Component, OnInit,Input,OnDestroy,Inject } from '@angular/core';
import { AppService } from '../../service/App.service'
import {Subscription } from 'rxjs';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MasterElement} from 'src/app/Model/Model';
export interface DialogData {
  index?:number;
  ModuleName?: string;
  Nooftool?: number;
}

@Component({
  selector: 'app-master-tools',
  templateUrl: './master-tools.component.html',
  styleUrls: ['./master-tools.component.scss']
})
export class MasterToolsComponent implements OnInit,OnDestroy {
  @Input() ToolType: string;
  sub:Subscription;
  Deletesub:Subscription;
  displayedColumns: string[] = ['position', 'ModuleName', 'Nooftools', 'editAction','deleteAction'];
  dataSource;

  
  constructor(private apps: AppService,public dialog: MatDialog ) {
   
   
   }
   openDialog(): void {
    const dialogRef = this.dialog.open(MasterAddEditDialog, {
      width: '260px',
      data: {index:-1,ModuleName:'',Nooftool:0}
    });

    dialogRef.afterClosed().subscribe(result => {
     // alert(result);
      console.log('The dialog was closed');
      //this.animal = result;
    });
  }

  EditDialog(temp:MasterElement,rowindex:number)
  {
    //console.log(temp);
    const dialogRef = this.dialog.open(MasterAddEditDialog, {
      width: '260px',
      data: {index:rowindex,ModuleName: temp.ModuleName,Nooftool: temp.Nooftools}
    });

    dialogRef.afterClosed().subscribe(result => {
     
     // console.log(result);
     this.apps.updateModule(this.ToolType,result.index,result.ModuleName,result.Nooftool)

      //this.animal = result;
    });

  }
  DeleteModuleClick(temp:MasterElement)
  {
    console.log(temp);
    this.Deletesub= this.apps.DeleteModule(this.ToolType,temp)
    .subscribe(Tools => this.dataSource = Tools);
    console.log(this.dataSource);
  }


  ngOnInit() {
    this.getTools(this.ToolType);
  }
  ngOnDestroy()
  {
      this.sub.unsubscribe();
  }

  

  getTools(ToolType:string): void {
   this.sub= this.apps.getTheTools(ToolType)
        .subscribe(Tools => this.dataSource = Tools);
  }

}
@Component({
  selector: 'MasterAddEdit',
  templateUrl: 'MasterAddEdit.html',
})
export class MasterAddEditDialog {

  constructor(
    public dialogRef: MatDialogRef<MasterAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}


