import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MasterTemplateComponent } from './master-template/master-template.component';
import { masterroutingmodule} from './master-routing.module';
import { MaterialModule} from './../material.module';
import { MasterToolsComponent,MasterAddEditDialog } from './master-tools/master-tools.component';



@NgModule({
  declarations: [MasterTemplateComponent, MasterToolsComponent,MasterAddEditDialog],
  entryComponents: [
    MasterAddEditDialog
  ],
  imports: [
    CommonModule,masterroutingmodule,MaterialModule,FormsModule
  ]
})
export class MasterModule { }
