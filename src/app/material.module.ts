import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule,MatTableModule  } from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import { 
  MatButtonModule, 
  MatIconModule, 
  MatMenuModule,
  MatToolbarModule,MatFormFieldModule
} from '@angular/material';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  imports: [ 
    MatButtonModule, 
    MatIconModule, CommonModule,
    MatMenuModule,MatInputModule,MatFormFieldModule,
    MatToolbarModule,MatTabsModule,MatTableModule ,MatDialogModule
  ],
  exports: [
    MatButtonModule, 
    MatIconModule, CommonModule,
    MatMenuModule,MatInputModule,MatFormFieldModule,
    MatToolbarModule,MatTabsModule,MatTableModule ,MatDialogModule
  ]    
})

export class MaterialModule {}