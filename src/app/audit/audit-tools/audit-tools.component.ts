import { Component, OnInit ,Input,OnDestroy} from '@angular/core';
import { AppService } from '../../service/App.service';
import { Subscription } from "rxjs"
import { MasterElement,Elementsum } from 'src/app/Model/Model';


@Component({
  selector: 'app-audit-tools',
  templateUrl: './audit-tools.component.html',
  styleUrls: ['./audit-tools.component.scss']
})
export class AuditToolsComponent implements OnInit,OnDestroy {
  @Input() ToolType: string;
  Sub: Subscription;
  dataSource:MasterElement[];
  constructor(private apps: AppService ) {
   
   
   }

  ngOnInit() {
    this.getTools(this.ToolType);
  }

  getTools(ToolType:string): void {
  this.Sub=  this.apps.getTheTools(ToolType)
        .subscribe(Tools => this.dataSource = Tools);
  }
  ngOnDestroy()
  {
      this.Sub.unsubscribe();
  }
 
  GetToolsumvalue(updateTool: Elementsum) {
    //console.log(updateTool);

    this.apps.UpdateToolSum(this.ToolType,updateTool.index,updateTool.sum,updateTool.elementSummary);
  }

}
