import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditToolsComponent } from './audit-tools.component';

describe('AuditToolsComponent', () => {
  let component: AuditToolsComponent;
  let fixture: ComponentFixture<AuditToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
