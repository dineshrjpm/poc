import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditToolrowComponent } from './audit-toolrow.component';

describe('AuditToolrowComponent', () => {
  let component: AuditToolrowComponent;
  let fixture: ComponentFixture<AuditToolrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditToolrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditToolrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
