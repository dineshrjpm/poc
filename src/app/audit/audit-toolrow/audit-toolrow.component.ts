import { Component, OnInit,Input,Output,EventEmitter} from '@angular/core';
import { MasterElement,ToolsElement,Elementsum} from '../../Model/Model'

@Component({
  selector: 'app-audit-toolrow',
  templateUrl: './audit-toolrow.component.html',
  styleUrls: ['./audit-toolrow.component.scss']
})
export class AuditToolrowComponent implements OnInit  {
  @Input() ToolName: MasterElement ;
  @Output() SumTool = new EventEmitter<Elementsum>();
  @Input() Toolindex:number;
  public Tools:ToolsElement[] =[];

  public Toolsum :number;
  constructor() {
   
   }
   
  ngOnInit() {

    this.arrayOne(this.ToolName.Nooftools)
  }
  arrayOne(n: number) {

    let words;
    if(this.ToolName.ToolSummary!=undefined)
    {
       words = this.ToolName.ToolSummary.split(',');
     // console.log(words);
    }
    //console.log(words)

    for(let i=0;i<n;i++)
    {
      if(words===undefined)
       this.Tools.push({position:i,Tool:'' })
       else
       this.Tools.push({position:i,Tool:words[i] })
    }
    if(this.ToolName.sum!=undefined)
    this.Toolsum= this.ToolName.sum
    
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if(charCode==48||charCode==49)   
    return true;
    else
    return false;

  }

  Onvalues(event): boolean {
    
    return false;

  }

  Sum(event) {
    var total = 0;
    this.Tools.filter(b=>b.Tool==="1").forEach(x => total += parseInt(x.Tool));
   this.Toolsum= total;

   var toolsummary=Array.prototype.map.call(this.Tools,function(item:any){ return item.Tool;}).join(",");
  //console.log(toolsummary);
   let temp:Elementsum={ index:this.Toolindex,sum:total,elementSummary:toolsummary }
   this.SumTool.emit(temp);
  
    //console.log(total);

    
   

  }
}
