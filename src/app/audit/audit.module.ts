import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { auditroutingmodule } from './audit-routing.module';
import { AuditTemplateComponent} from './audit-template/audit-template.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';
import { AuditToolsComponent } from './audit-tools/audit-tools.component';
import { MaterialModule} from './../material.module';
import { AuditToolrowComponent } from './audit-toolrow/audit-toolrow.component';
@NgModule({
  declarations: [
    AuditTemplateComponent,
    AuditToolsComponent,
    AuditToolrowComponent
  ],
  imports: [
    CommonModule,auditroutingmodule,FormsModule,
    ReactiveFormsModule,MaterialModule
    
  ]
})
export class AuditModule { }
