import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditTemplateComponent} from './audit-template/audit-template.component';



const routes: Routes = [
  { path: '',  component: AuditTemplateComponent}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class auditroutingmodule { }
